package com.mygdx.game.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Kamalov on 01.11.2015.
 */
public class Stage_1  {

    private TmxMapLoader mapLader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    //private World world;
    //private Box2DDebugRenderer b2dr;
    private OrthographicCamera cam;
    //private List<Rectangle> listRectangles;
    private ArrayList<Rectangle> listRectangle =new ArrayList<Rectangle>();





    public Stage_1(){
        tmxLoder("Level1.tmx");

        //box2dLoader();
    }

    private void tmxLoder(String levelFile){
        mapLader=new TmxMapLoader();
        map=mapLader.load(levelFile);
        renderer=new OrthogonalTiledMapRenderer(map);
        loadObjects();
    }


    //.get(4).getObjects().getByType(PolylineMapObject.class)
    private void loadObjects(){
        for (MapObject object: map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            //System.out.println("Vasya= "+rect);
            this.listRectangle.add(rect);
        }
    }

    public ArrayList<Rectangle> getRectangle(){
        return listRectangle;
    }
/*
    private void box2dLoader(){
        world=new World(new Vector2(0,0),true);
        b2dr=new Box2DDebugRenderer();

        BodyDef bdef=new BodyDef();
        PolygonShape shape= new PolygonShape();
        FixtureDef fdef=new FixtureDef();
        Body body;

        for (MapObject object: map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type=BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX()+rect.getWidth()/2,rect.getY()+rect.getHeight()/2);

            body =world.createBody(bdef);
            shape.setAsBox(rect.getWidth()/2,rect.getHeight()/2);
            fdef.shape=shape;
            body.createFixture(fdef);
        }
    }
*/

    public void setCam(OrthographicCamera cam){
        this.cam=cam;
        renderer.setView(cam);
    }
    //render to display
    public void render(){
        renderer.render();
    }

    /*
    public void b2dRender(){
        b2dr.render(world, this.cam.combined);
    }
    public World getWorld(){
        return world;
    }
    */
}
