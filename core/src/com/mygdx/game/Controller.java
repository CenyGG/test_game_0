package com.mygdx.game;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.mygdx.game.Screens.MainMenuScreen;
import com.mygdx.game.Sprites.Player;
import com.mygdx.game.Stages.Stage_1;

/**
 * Created by Kamalov on 02.11.2015.
 */
public class Controller {

    //region Game Area params
    private final float LEFTBORDER=9;
    private final float RIGHTBORDER=788;
    private final float TOPBORDER=468;
    private final float BOTTOMBORDER =6;

    private final float LEFT_BTN_X_BORDER=167;
    private final float LEFT_BTN_Y_BORDER=125;

    private final float RIGHT_BTN_X_BORDER=636;
    private final float RIGHT_BTN_Y_BORDER=125;
    //endregion

    private final static float SPEED=300;
    private Touchpad touchpad;
    private Button button;
    private Player player;
    private Stage_1 stage;
    private MainMenuScreen screen;

    public boolean BUTTON_IS_PRESSED=false;

    public Controller(Touchpad touchpad, Button button,Player player, Stage_1 stage,MainMenuScreen screen){
        this.touchpad=touchpad;
        this.button=button;
        this.player=player;
        this.stage=stage;
        this.screen=screen;

    }

    public void update(float dt){


        //region Handle Input
        //player.setPosition(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
        if (!button.isPressed()){
            BUTTON_IS_PRESSED=false;

            if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()<0) {
                if(player.isContainedInAnyRectangles(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED))
                    player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
                player.move = Player.Move.DOWN;
            }
            else if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()>0) {
                if(player.isContainedInAnyRectangles(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED))
                    player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
                player.move = Player.Move.UP;
            }
            else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()<0){
                if(player.isContainedInAnyRectangles(player.getX()+touchpad.getKnobPercentX()*dt * SPEED, player.getY()))
                    player.setPosition(player.getX()+touchpad.getKnobPercentX()*dt * SPEED, player.getY());
                player.move = Player.Move.LEFT;

            }
            else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()>0) {
                if(player.isContainedInAnyRectangles(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY()))
                    player.setPosition(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY());
                player.move = Player.Move.RIGHT;
            }
        }else if(button.isPressed()){
            BUTTON_IS_PRESSED=true;

            float tempStartX= player.getX();
            float tempStartY=player.getY();

            if(player.getCentralX()<LEFTBORDER)
                player.setCentralX(LEFTBORDER);
            else if(player.getCentralX()>RIGHTBORDER)
                player.setCentralX(RIGHTBORDER);
            else if(player.getCentralY()>TOPBORDER)
                player.setCentralY(TOPBORDER);
            else if(player.getCentralY()< BOTTOMBORDER)
                player.setCentralY(BOTTOMBORDER);
            if(player.getCentralX()<LEFT_BTN_X_BORDER&&player.getCentralY()<LEFT_BTN_Y_BORDER){
                if(player.move== Player.Move.LEFT)
                    player.setCentralX(LEFT_BTN_X_BORDER);
                else if(player.move== Player.Move.DOWN)
                    player.setCentralY(LEFT_BTN_Y_BORDER);
            }else if(player.getCentralX()>RIGHT_BTN_X_BORDER&&player.getCentralY()<RIGHT_BTN_Y_BORDER) {
                if (player.move == Player.Move.RIGHT)
                    player.setCentralX(RIGHT_BTN_X_BORDER);
                else if (player.move == Player.Move.DOWN)
                    player.setCentralY(RIGHT_BTN_Y_BORDER);
            }

            if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()<0) {
                player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
                player.move = Player.Move.DOWN;
            }
            else if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()>0) {
                player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
                player.move = Player.Move.UP;
            }
            else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()<0){
                player.setPosition(player.getX()+touchpad.getKnobPercentX()*dt * SPEED, player.getY());
                player.move = Player.Move.LEFT;

            }
            else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()>0) {
                player.setPosition(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY());
                player.move = Player.Move.RIGHT;
            }

            //screen.renderLine(tempStartX,tempStartY,player.getX(),player.getY());
            //screen.renderLine(0,0,400,400);;
        }
        //endregion



        player.getPlayerCircle().setPosition(player.getX(),player.getY());
    }

}
