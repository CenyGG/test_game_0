package com.mygdx.game.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Sprites.Player;


/**
 * Created by Kamalov on 29.10.2015.
 */
public class Hud implements Disposable {

    private final static float BUTTONSIZE=100;
   // private final static float SPEED=300;
   // public final float border_Height;
   // public final float border_Width;


    public Stage stage;
    private Viewport viewport;
    Player player;

    public Touchpad getTouchpad() {
        return touchpad;
    }

    private Touchpad touchpad;
    private TouchpadStyle touchpadStyle;
    private Skin skin;

    public Button getButton() {
        return button;
    }

    private Button button;
    private ButtonStyle buttonStyle;

    private Image leftButtonBackGround;
    private Image rightButtonBackGround;

    TextureAtlas atlas;



    public Hud(Player player, SpriteBatch sb) {
        viewport = new FitViewport(MyGdxGame.WIDTH, MyGdxGame.HIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);
        this.player=player;


        atlas=new TextureAtlas(Gdx.files.internal("Controller.pack"));


        //Create a skin
        skin = new Skin();
        skin.addRegions(atlas);


        touchpadStyle = new TouchpadStyle();
        touchpadStyle.background = skin.getDrawable("Knob1");
        touchpadStyle.knob = skin.getDrawable("Knob2");



        touchpad = new Touchpad(10, touchpadStyle);
        touchpad.setBounds(36, 13, BUTTONSIZE, BUTTONSIZE);


        buttonStyle=new ButtonStyle();
        buttonStyle.up = skin.getDrawable("simple");
        buttonStyle.down = skin.getDrawable("CircleBtn");

        button=new Button(buttonStyle);
        button.setBounds(MyGdxGame.WIDTH-BUTTONSIZE-32,13,BUTTONSIZE,BUTTONSIZE);

/*
        Texture ltexture=new Texture("borders.png");
        this.border_Height=ltexture.getHeight();
        this.border_Width=ltexture.getWidth();
        leftButtonBackGround=new Image(ltexture);
        leftButtonBackGround.setBounds(0, 0, border_Width, border_Height);


        Texture rtexture=new Texture("borders1.png");
        rightButtonBackGround=new Image(rtexture);
        rightButtonBackGround.setBounds(MyGdxGame.WIDTH -border_Width,0,border_Width,border_Height);

        stage.addActor(leftButtonBackGround);
        stage.addActor(rightButtonBackGround);

        */

        stage.addActor(touchpad);
        stage.addActor(button);
        Gdx.input.setInputProcessor(stage);



        //table.setFillParent(true);

        Gdx.input.setInputProcessor(stage);
        //font = new BitmapFont();


    }

    @Override
    public void dispose() {

    }



    public void onResize(int width, int height){
        stage.getViewport().update(width, height, true);
    }

    public void update(float dt){
        //player.setPosition(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY() + touchpad.getKnobPercentY() * dt * SPEED);

        /*
        if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()<0) {
            player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
            player.move = Player.Move.DOWN;
        }
        else if(Math.abs(touchpad.getKnobPercentX())<Math.abs(touchpad.getKnobPercentY()) && touchpad.getKnobPercentY()>0) {
            player.setPosition(player.getX(), player.getY() + touchpad.getKnobPercentY() * dt * SPEED);
            player.move = Player.Move.UP;
        }
        else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()<0){
            player.setPosition(player.getX()+touchpad.getKnobPercentX()*dt * SPEED, player.getY());
            player.move = Player.Move.LEFT;

        }
        else if(Math.abs(touchpad.getKnobPercentX()) >Math.abs(touchpad.getKnobPercentY())&&touchpad.getKnobPercentX()>0) {
            player.setPosition(player.getX() + touchpad.getKnobPercentX() * dt * SPEED, player.getY());
            player.move = Player.Move.RIGHT;
        }


        player.getPlayerCircle().setPosition(player.getX(),player.getY());


*/

        //System.out.println(String.format("MOVE: %s",player.move));
        //player.b2body.setLinearVelocity(player.getX() + touchpad.getKnobPercentX()*dt*SPEED, player.getY() + touchpad.getKnobPercentY()*dt*SPEED);
    }
}
