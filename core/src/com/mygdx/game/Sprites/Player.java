package com.mygdx.game.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Screens.MainMenuScreen;
import com.mygdx.game.Stages.Stage_1;

import java.util.HashMap;


/**
 * Created by Kamalov on 30.10.2015.
 */



public class Player extends Sprite {
    public static final float RADIUS=16;
    public static final float MIN_DISTANSE=1;
    public enum State { SECURE, UNSECURE };
    public enum Move {UP,DOWN,LEFT,RIGHT,STOP};
    public boolean DRAWING=false;
    public boolean END_DRAWING=false;

    public State state = State.SECURE;
    public Move move = Move.STOP;
    public HashMap<Rectangle,Float> distanses=new HashMap<Rectangle, Float>();
   // public HashSet<Move> canMove = new HashSet<Move>();

    //public World world;
    //public Body b2body;

    //public float centralX =getX()+getWidth()/2;
    //public float centralY =getY()+getHeight()/2;

    private MainMenuScreen screen;

    private Circle playerCircle;

    public Circle getPlayerCircle() {
        return playerCircle;
    }

    public Player(MainMenuScreen screen){
        super(new Texture("player_obj.png"));

        this.screen= screen;
        //this.world=world;
        //definePlayer();

        //texture=new Texture("simple.png");
        //setTexture(new Texture("simple.png"));
        setPosition(MyGdxGame.WIDTH / 2, 0);

        playerCircle =new Circle();
        playerCircle.setRadius(RADIUS);



    }

    public float getCentralX(){
        return getX()+getWidth()/2;
    }
    public float getCentralY(){
        return getY()+getHeight()/2;
    }
    public void setCentralX(float x){
        setX(x-getWidth()/2);
    }
    public void setCentralY(float y){
        setY(y - getHeight() / 2);
    }
/*
    private float xDistanceToRectangle(Rectangle rect){
        return (rect.getX()> centralX)?rect.getX()- centralX : centralX -(rect.getX()+rect.getWidth());
    }

    private float yDistanceToRectangle(Rectangle rect){
        return (rect.getY()> centralY)?rect.getY()- centralY : centralY -(rect.getY()+rect.getHeight());
    }
*/
    public boolean isContainedInAnyRectangles(float x,float y){
        x+=getWidth()/2;
        y+=getHeight()/2;
        for(Rectangle rect:screen.stage_1.getRectangle()){
            if(rect.contains(x,y)){
                return true;
            }
        }
        return false;
    }


/*
    private void definePlayer(){
        BodyDef bdef=new BodyDef();
        bdef.position.set(MyGdxGame.HIGHT/2,MyGdxGame.WIDTH/2);
        bdef.type=BodyDef.BodyType.DynamicBody;
        b2body=world.createBody(bdef);


        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(RADIUS);

        fdef.shape=shape;
        b2body.createFixture(fdef);

    }
*/
    public void update(float dt){

        if(screen.controller.BUTTON_IS_PRESSED && !isContainedInAnyRectangles(getX(), getY())){
            DRAWING=true;
            END_DRAWING=false;
        }else if(DRAWING &&isContainedInAnyRectangles(getX(), getY())){
            DRAWING=false;
            END_DRAWING=true;
        }else{
            DRAWING=false;
            END_DRAWING=false;
        }

        //update our sprite to correspond with the position of our Box2D body
        //setPosition(getX(), getY());
        //System.out.println(String.format("GetX: %s, GetY: %s",getX(),getY()));
/*
        if(getX()<0)
            setX(0);
        else if(getX()>MyGdxGame.WIDTH-this.getWidth())
            setX(MyGdxGame.WIDTH - this.getWidth());
        if(getY()<0)
            setY(0);
        else if (getY()>MyGdxGame.HIGHT-this.getHeight())
            setY(MyGdxGame.HIGHT - this.getHeight());
*/

/*
        for(Rectangle rect:stage.getRectangle()){

            float tempX=this.xDistanceToRectangle(rect);
            float tempY=this.yDistanceToRectangle(rect);
            float tempD= tempX>tempY ? tempY:tempX;
            distanses.put(rect,tempD);

            if(this.move==Move.RIGHT && rect.contains(this.getX()+getWidth()/2,this.getY()+getHeight()/2)){
                this.setX(rect.getX()-getWidth()/2-1);
                //if()
            }
            if(this.move==Move.LEFT && rect.contains(this.getX()+getWidth()/2,this.getY()+getHeight()/2))
                this.setX(rect.getX()+rect.getWidth()-getWidth()/2+1);
            if(this.move==Move.UP && rect.contains(this.getX()+getWidth()/2,this.getY()+getHeight()/2))
                this.setY(rect.getY()-getHeight()/2-1);
            if(this.move==Move.DOWN && rect.contains(this.getX()+getWidth()/2,this.getY()+getHeight()/2))
                this.setY(rect.getY()+rect.getHeight()-getWidth()/2+1);



        }
*/




    }

}
