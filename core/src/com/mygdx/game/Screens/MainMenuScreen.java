package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ShortArray;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Controller;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Scenes.Hud;
import com.mygdx.game.Screens.GameScreen;
import com.mygdx.game.Sprites.Player;
import com.mygdx.game.Stages.Stage_1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

class MyPoint{
    public float x;
    public float y;
    public MyPoint(float x, float y){
        this.x=x;
        this.y=y;
    }
}

class MyLine{
    public float x;
    public float y;
    public float x1;
    public float y1;
    public MyLine(float x, float y,float x1, float y1){
        this.x=x;
        this.y=y;
        this.x1=x1;
        this.y1=y1;
    }
}


public class MainMenuScreen implements Screen {

    final MyGdxGame game;
    OrthographicCamera camera;
    //Texture texture;
    Viewport viewport;
    Hud hud;
    Player player;
    public Stage_1 stage_1;
    public Controller controller;
    //World world;
    ShapeRenderer shapeRenderer=new ShapeRenderer();
    List<MyPoint> listPoints=new ArrayList<MyPoint>();

    List<ArrayList<MyLine>> arrayOfLines=new ArrayList<ArrayList<MyLine>>();

    public boolean START_DRAWING=false;




    public MainMenuScreen(final MyGdxGame gam) {
        game = gam;



        stage_1=new Stage_1();
       // world=stage_1.getWorld();

        player=new Player(this);
        //listPoints.add(new MyPoint(player.getCentralX(),player.getCentralY()));

        //texture=new Texture("y9WJTF5KFyE.jpg");
        camera = new OrthographicCamera();

        camera.setToOrtho(false, gam.WIDTH, gam.HIGHT);
        hud=new Hud(player,gam.batch);
        stage_1.setCam(camera);
        controller=new Controller(hud.getTouchpad(),hud.getButton(),player,stage_1,this);


    }


    private void drawTriangles(List<ArrayList<MyLine>> listArrayLines){
        for(ArrayList<MyLine> lines:listArrayLines){
            this.renderTriangle(lines);
        }
    }

    //region ShapeRenreres
    private void renderRectangle(Rectangle rect){
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1, 1, 0, 1);
        shapeRenderer.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        shapeRenderer.end();

    }

    private void renderListRectangles(ArrayList<Rectangle> rectList){
        for(Rectangle rect:rectList){
            renderRectangle(rect);
        }
    }

    public  void  renderLine(float x,float y,float x1,float y1){
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 1, 0, 1);
        shapeRenderer.line(x, y, x1, y1);
        shapeRenderer.end();
        //System.out.println(String.format("Line: from %s %s - to %s %s", x, y, x1, y1));
    }

    public  void  renderLine(MyLine line){
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(0, 1, 0, 1);
        shapeRenderer.line(line.x, line.y, line.x1, line.y1);
        shapeRenderer.end();
    }
    public void renderLines(ArrayList<MyLine> lines){
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(0, 1, 0, 1);
        for(MyLine line:lines){
            shapeRenderer.line(line.x, line.y, line.x1, line.y1);
        }
        shapeRenderer.end();
    }

    public  void  renderPoint(float x,float y){
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
        shapeRenderer.setColor(1, 1, 0, 1);
        shapeRenderer.point(x, y, 0);
        shapeRenderer.end();
        //System.out.println(String.format("Point: from %s %s ", x, y));
    }
    public  void  renderPoint(MyPoint point){
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
        shapeRenderer.setColor(1, 1, 0, 1);
        shapeRenderer.point(point.x, point.y, 0);
        shapeRenderer.end();
        //System.out.println(String.format("Point: from %s %s ", x, y));
    }

    public void renderTriangle(ArrayList<MyLine> lines){
        EarClippingTriangulator triangulator = new EarClippingTriangulator();
        shapeRenderer.setProjectionMatrix(camera.combined);


        List<Float> vertices=new ArrayList<Float>();
        //ArrayList<MyLine> lines=getLines();
        if(!lines.isEmpty()){
            vertices.add(lines.get(0).x);
            vertices.add(lines.get(0).y);
        }
        for(MyLine line:lines){
            vertices.add(line.x1);
            vertices.add(line.y1);
        }
        float[] flotVertexes=new float[vertices.size()];
        int i=0;
        for(Float flt:vertices){
            flotVertexes[i]=flt;
            i++;
        }

        ShortArray triangles = triangulator.computeTriangles(flotVertexes);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        Random random=new Random();
        shapeRenderer.setColor(Color.CORAL);
        for (int j = 0; j < triangles.size; j += 3) {
            int p1 = triangles.get(j) * 2;
            int p2 = triangles.get(j + 1) * 2;
            int p3 = triangles.get(j + 2) * 2;
            shapeRenderer.triangle( //
                    flotVertexes[p1], flotVertexes[p1 + 1], //
                    flotVertexes[p2], flotVertexes[p2 + 1], //
                    flotVertexes[p3], flotVertexes[p3 + 1] //
            );
        }

        shapeRenderer.setColor(Color.GREEN);
        int last = flotVertexes.length - 2;
        for (int j = 0; j < flotVertexes.length; j += 2) {
            shapeRenderer.line(flotVertexes[j], flotVertexes[j + 1], flotVertexes[last], flotVertexes[last + 1]);
            last = j;
        }

        shapeRenderer.end();

    }

//    public void renderPolygon(){
//        PolygonSprite poly;
//        PolygonSpriteBatch polyBatch;
//        Texture textureSolid;
//        EarClippingTriangulator triangulator=new EarClippingTriangulator();
//        polyBatch = new PolygonSpriteBatch();
//        polyBatch.setProjectionMatrix(camera.combined);
//
//
//
//        Pixmap pix = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
//        textureSolid = new Texture(pix);
//        shapeRenderer.setProjectionMatrix(camera.combined);
//        List<Float> vertices=new ArrayList<Float>();
//        ArrayList<MyLine> lines=getLines();
//        if(!lines.isEmpty()){
//            vertices.add(lines.get(0).x);
//            vertices.add(lines.get(0).y);
//        }
//        for(MyLine line:lines){
//            vertices.add(line.x1);
//            vertices.add(line.y1);
//        }
//        float[] flotVertexes=new float[vertices.size()];
//        int i=0;
//        for(Float flt:vertices){
//            flotVertexes[i]=flt;
//            i++;
//        }
//
//        ShortArray triangles = triangulator.computeTriangles(flotVertexes);
//        short[] arrayTriangles = triangles.toArray();
//        if(vertices.size()>4) {
//
//            PolygonRegion polyReg = new PolygonRegion(new TextureRegion(textureSolid),flotVertexes,arrayTriangles);
//            poly = new PolygonSprite(polyReg);
//
//
//            polyBatch.begin();
//            poly.draw(polyBatch);
//            polyBatch.end();
//        }
//
//
//    }
    //endregion



    //Get ArrayList<MyLine> from this.listPoints
    public ArrayList<MyLine> getLines(){
        float xStart=listPoints.get(0).x;
        float yStart=listPoints.get(0).y;
        float xEnd=listPoints.get(0).x;
        float yEnd=listPoints.get(0).y;
        ArrayList<MyLine> listLines=new ArrayList<MyLine>();

        for(int i=0;i<listPoints.size()-1;i++)
        {

            if(listPoints.get(i).x==listPoints.get(i+1).x){
                while(i<listPoints.size()-1&&listPoints.get(i).x==listPoints.get(i+1).x ){
                    yEnd=listPoints.get(i+1).y;
                    i++;
                }
                xEnd=listPoints.get(i).x;
                listLines.add(new MyLine(xStart,yStart,xEnd,yEnd));
            }else if(listPoints.get(i).y==listPoints.get(i+1).y){
                while(i<listPoints.size()-1&&listPoints.get(i).y==listPoints.get(i+1).y&& i<listPoints.size()-1){
                    xEnd=listPoints.get(i+1).x;
                    i++;
                }
                yEnd=listPoints.get(i).y;
                listLines.add(new MyLine(xStart,yStart,xEnd,yEnd));
            }
            xStart=xEnd;
            yStart=yEnd;
        }

        return listLines;
    }



    @Override
    public void show() {

    }

    public void update(float dt){


        /*

        for(MyPoint point:listPoints){
            renderPoint(point);
        }
        */
        /*
        for(MyLine line:getLines()){
            renderLine(line);
        }
        */
        drawTriangles(arrayOfLines);

        if(player.DRAWING){
            listPoints.add(new MyPoint(player.getCentralX(),player.getCentralY()));
            renderLines(getLines());
        }else if(player.END_DRAWING){
            arrayOfLines.add(getLines());
            listPoints.clear();
        }


    }

    @Override
    public void render(float delta) {


        Gdx.gl.glClearColor(0, 0, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);



        //hud.update(delta);
        // stage_1.b2dRender();

        controller.update(delta);
        stage_1.render();


        renderListRectangles(stage_1.getRectangle());

        player.update(delta);
        update(delta);

        //renderPolygon();
        //renderPoint(player.getX(), player.getY());
        //System.out.println(String.format("X: %s Y: %s", player.getCentralX(), player.getCentralY()));
        //System.out.println(String.format("Sprite %s   |%s", player.getX(), player.getY()));
        //System.out.println(String.format("Circle %s   |%s", player.getPlayerCircle().x, player.getPlayerCircle().y));
        //System.out.println(player.getTexture().getWidth());

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);


        game.batch.begin();


        //game.batch.draw(texture, 0, 0);
        //game.font.draw(game.batch, "Welcome to Drop!!! ", 100, 150);
        //game.font.draw(game.batch, "Tap anywhere to begin!", 100, 100);
        //game.batch.draw(player.getTexture(),player.getX(),player.getY());

        player.draw(game.batch);


        game.batch.end();


        hud.stage.draw();


        //game.batch.setProjectionMatrix(hud.stage.getCamera().combined);

/*
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }*/
    }

    @Override
    public void resize(int width, int height) {
        hud.onResize(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    //Rest of class still omitted...

}